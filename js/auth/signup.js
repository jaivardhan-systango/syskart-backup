const db = firebase.firestore();

function createUser(name, email, password, type) {
    firebase.auth().createUserWithEmailAndPassword(email, password)
        .then((result) => {
            console.log(result.user.uid);
            sendMail();
            saveUserDetails(result.user.uid, name, type);
            
            $("#success").css("display", "block");
        })
        .catch(function (error) {
            var errorMessage = error.message;

            // Sending error message to ui
            $("#error").html(errorMessage);
            $("#error").css("display", "block");
        });
}

function sendMail() {
    firebase.auth().currentUser.sendEmailVerification()
        .then(function () {
            console.log("Successful")
        })
        .catch(function (error) {
            console.log(error);
        });
}

function saveUserDetails(user_id, name, type) {

    db.collection("user_details").add({
        user_id,
        name,
        type,
        creation_date: new Date()
    }).then(function (docRef) {
        console.log("Document written with ID: ", docRef.id);
    }).catch(function (error) {
        console.error("Error adding document: ", error);
    });
}

function signup() {
    var name = $("#full_name").val().trim();
    var email = $("#email").val().trim();
    var password = $("#password").val();
    var re_password = $("#re-password").val();
    var type = $("#user_type_select").val();

    if(name==="" || email==="" || password==="" || type==="0") {
        $("#error").html("Fields can't be empty.");
        $("#error").css("display", "block");
    }else if(password!==re_password){
        $("#error").html("Password didn't match");
        $("#error").css("display", "block");
    }else{
        $("#error").css("display", "none");
        createUser(name, email, password, type);
    }
}

$("#re-password").keyup(() => {
    var password = $("#password").val();
    var re_password = $("#re-password").val();
    
    if(password!==re_password){
        $("#re-error").html("Password didn't match");
        $("#re-error").css("display", "block");
    }else{
        $("#re-error").css("display", "none");
    }
})

function getTypes() {
    db.collection("user_type").get().then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
            let data = doc.data();
            $("#user_type_select").append(`<option value="${data.type}">${data.type}</option>`)
        });
    });
}

$("#email").change(function () {
    var email = $("#email").val()
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(!re.test(String(email).toLowerCase())) {
        $("#error").html("The email address is badly formatted");
        $("#error").css("display", "block");
    }else{
        $("#error").css("display", "none");
    }
});

getTypes();