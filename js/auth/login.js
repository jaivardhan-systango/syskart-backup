function authenticateUser(email, password) {
    firebase.auth().signInWithEmailAndPassword(email, password)
        .then((result) => {
            if (result.user.emailVerified) {
                localStorage.setItem("current_user", result.user.uid);
                $("#error").css("display", "none");
                return Promise.resolve(result.user.uid)
            } else {
                return Promise.reject({
                    message: "Please verify your email"
                });
            }
        }).then((uid) => {
            const db = firebase.firestore();
            return db.collection("user_details").where("user_id", "==", uid).limit(1).get()
        }).then((querySnapshot) => {
            querySnapshot.forEach(function (doc) {
                localStorage.setItem("user_type", doc.data().type);
            });

            $(location).attr('href', '../templates/products.html');
        })
        .catch(function (error) {
            var errorMessage = error.message;
            $("#error").html(errorMessage);
            $("#error").css("display", "block");
        });
}

function login(event) {
    const email = $('#email').val()
    const password = $('#password').val()
    authenticateUser(email, password);
}